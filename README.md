# README #

Creators:
Erin Winter
Gordon Loery
Simphiwe Hlophe

# Summary #

CarlWeather is a weather service for the Carleton community. It displays current weather data collected from the weather equipment on top of Olin Hall (temperature, wind chill, etc.). The app also shows other information taken from the weather.carleton.edu website, including upcoming moon cycles, sunset/sunrise times, and graphs depicting temperature and barometric over timed intervals. The information is divided into The data displayed is updated at 2 minute intervals on average. The weather.carleton.edu page is detailed, but somewhat old, both technically and visually. We designed the app to display the same data in a more intuitive fashion on a mobile platform.

# Project Status #

We had an ongoing bug involving the display of outdated, cached fragments that we fixed through a workaround. We realized our problem came from internal mechanisms of the Android ViewPager class. The ViewPager class is designed to work with sliding tabs, so the user can push and pull screens from neighboring tabs. This means that the ViewPager runs three fragments simultaneously at any given time: the currently selected tab, its left neighbor, and its right neighbor. We first designed the app with the “HOME” and “DETAILS” tabs as the 1st and 2nd tab, because the details screen expands on the information summarized in the home tab. However, this created problems in changing data across tabs because the ViewPager’s design necessitated that the home tab was always running when the details tab was and vice-versa. We couldn’t change how the ViewPager interacted with the fragments without losing some of the screen-sliding functionality. As a workaround, we swapped the positions of the “SUN” and “DETAILS” tabs. This way, because the details tab is not an immediate neighbor, a fresh copy of the detail screen is always loaded when changes have been made to the main screen, like switch between Fahrenheit and Celsius.

We originally planned to have a dynamically changing background that shifted with changes in temperature. This was easy to implement, but in practice, the additional Bitmaps stored and loaded in memory at runtime made the application staggeringly slow. So, we nixed the feature in favor of a more stable app.


# Getting Started #

We haven’t had any difficult loading the packaged libraries across computers, but our version of the project uses the Gradle settings associated with Android Studio 0.5.2 for Windows. We haven’t been able to get it to build successfully on the Mac version of Android Studio or earlier builds of Android Studio for Windows.


